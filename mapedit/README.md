# London 1940 - London Architecture Simulation Software

I have set up a project called London 1940 which simulates London in 1940. I'm a British Citizen and I have been writing open source simulation software since 1996. I have the maps of London through the Library of Scotland's Ordinance Survey Maps of London in 1940 and surrounds and I have been working on generating JSON maps of London so the simulated Londoners can move around.

This is [me](https://www.linkedin.com/in/barbalet/).

This is gitlab validating my [open source work](https://gitlab.com/apesdk).

This is the [London 1940 project source](https://gitlab.com/apesdk/london1940).

This is the [YouTube channel](https://www.youtube.com/channel/UC2LEGKQLYv524CLMugmm7rw).

Tom Barbalet. [London 1940 Website](https://london1940.org/)
