/****************************************************************

 newlondon.c

 =============================================================

 Copyright 1996-2024 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include "pnglite.h"
#include "toolkit.h"
#include <stdio.h>
#include <stdlib.h>

n_int draw_error( n_constant_string error_text, n_constant_string location, n_int line_number )
{
    printf( "ERROR: %s @ %s %ld\n", ( const n_string ) error_text, location, line_number );
    return -1;
}


n_uint newlondon_m(n_uint x, n_uint y, n_uint width, n_uint height, n_uint count) {
    return (n_uint)((x + (y * width))*3) + count;
}

void newlondon_process(n_string input, n_string output, n_int width, n_int height) {

    n_byte *band = memory_new(width * height * 3);
    png_t png;
    n_byte *buffer = read_png_file(input, &png);
    
    memory_copy(buffer, band, width * height * 3);
    
    n_int loopy = 1;
    while (loopy < height) {
        n_int loopx = 1;
        while (loopx < width) {
            n_int clear = 0;
            n_uint p01, p10, p00;

            p01  = buffer[newlondon_m(loopx, loopy-1, width, height, 0)];
            p01 += buffer[newlondon_m(loopx, loopy-1, width, height, 1)];
            p01 += buffer[newlondon_m(loopx, loopy-1, width, height, 2)];

            p10  = buffer[newlondon_m(loopx-1, loopy, width, height, 0)];
            p10 += buffer[newlondon_m(loopx-1, loopy, width, height, 1)];
            p10 += buffer[newlondon_m(loopx-1, loopy, width, height, 2)];

            p00  = buffer[newlondon_m(loopx, loopy, width, height, 0)];
            p00 += buffer[newlondon_m(loopx, loopy, width, height, 1)];
            p00 += buffer[newlondon_m(loopx, loopy, width, height, 2)];
            

            if (((p01+p10)/150) != (p00/75)) {
                band[newlondon_m(loopx, loopy, width, height, 0)] = 0;
                band[newlondon_m(loopx, loopy, width, height, 1)] = 0;
                band[newlondon_m(loopx, loopy, width, height, 2)] = 0;
            }else{
                band[newlondon_m(loopx, loopy, width, height, 0)] = 255;
                band[newlondon_m(loopx, loopy, width, height, 1)] = 255;
                band[newlondon_m(loopx, loopy, width, height, 2)] = 255;
            }
            
//            if (((p01+p10)/80) == (p00/40)) {
//                band[newlondon_m(loopx, loopy, width, height, 0)] = 255;
//                band[newlondon_m(loopx, loopy, width, height, 1)] = 255;
//                band[newlondon_m(loopx, loopy, width, height, 2)] = 255;
//            }
            
            loopx ++;
        }
        loopy ++;
    }
    (void)write_png_file(output, width, height, band);
    memory_free( (void**) &band );
}

int main(int argc, const char * argv[]) {
    newlondon_process("new_maidstone.png", "old_maidstone.png", 2106, 1257);
    newlondon_process("new_canterbury.png", "out_canterbury.png", 1648, 1539);
    newlondon_process("new_ashford.png", "out_ashford.png", 2174, 1754);
    return 0;
}
