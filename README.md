## DESCRIPTION

Simulated Ape (formerly known as *Nervana* and *Noble Ape*) has been in development since 1996.

It features a number of autonomous simulation components including:

* landscape simulation, 
* biological simulation,
* weather simulation,
* sentient creature simulation (including a number of internal biological simulations), and,
* a simple intelligent-agent scripting language (ApeScript).

Build instructions can be found in /apesdk/build.html

## CONTENTS

This project contains:

* Simulated Ape
* Simulated Planet (planet)
* Simulated Urban (urban, UI deprecated now through command line PNG output)
* Simulated War (war)

With the same ApeSDK!

Also for the London 1940 development:

* drawout
* mapedit


