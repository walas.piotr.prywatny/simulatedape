/****************************************************************
 
ASMacCG.swift
 
 =============================================================
 
 Copyright 1996-2024 Tom Barbalet. All rights reserved.
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.
 
 ****************************************************************/

import Cocoa

@objc class ASMacCG: NSObject {
    
    var shared: ASShared
    
    public init(shared: ASShared) {
        self.shared = shared
    }
    
    public func quitProcedure() {
        shared.close()
        
        exit(0)
    }
    
    public func drawRect(rect: NSRect) {
        let dim_x: Int = Int(rect.size.width)
        let dim_y: Int = Int(rect.size.height)
        shared.cycle()
        
        if shared.cycleQuit()
        {
            print("Quit procedure initiated")
            quitProcedure()
        }
        
        if shared.cycleNewApes()
        {
            print("New apes neede to continue simulation")
            shared.newAgents()
        }
        
        let colorSpace: CGColorSpace = CGColorSpaceCreateDeviceRGB()
//
        var drawRef: CGContext = CGContext.init(data: shared_draw(shared.sharedId(), dim_x, dim_y, 0), width: dim_x , height: dim_y, bitsPerComponent: 8, bytesPerRow: dim_x * 4, space: colorSpace, bitmapInfo: UInt32(CGBitmapInfo.byteOrder32Big.rawValue|CGImageAlphaInfo.noneSkipFirst.rawValue))!
//
        var context: CGContext = NSGraphicsContext.current!.cgContext
//
//
        context.saveGState()
//        CGBlendMode(codingKey: copy)
//
//
//        CGBlendMode(context, kCGBlendModeCopy);

//
        let image:CGImage = drawRef.makeImage()!
//        
        context.draw(image, in: rect)
        context.restoreGState();

    }
    
}
