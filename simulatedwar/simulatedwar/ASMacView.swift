/****************************************************************
 
 ASMacView.swift
 
 =============================================================
 
 Copyright 1996-2024 Tom Barbalet. All rights reserved.
 
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:
 
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.
 
 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.
 
 ****************************************************************/

import Cocoa

@objc class ASMacView: ASMacCG {
    
//    var drawRef: CGContext
//    var displayLink: CVDisplayLink
//    var window: NSWindow
    
    public override init(shared: ASShared) {
        super.init(shared: shared)
    }
    
    @objc public func sharedReady() {
        
    }
        
    
//    public func renderCallback(displayLink:CVDisplayLink, timestamp: UnsafePointer<CVTimeStamp>, timestamp2: UnsafePointer<CVTimeStamp>, UInt64, UnsafeMutablePointer<UInt64>, Optional<UnsafeMutableRawPointer>) -> Int32
//    {
//        return Int32(ASMacView(displayLinkContext, renderTime:inOutputTime))
//    }
    
    public func renderTime(inOutputTime: CVTimeStamp) -> CVReturn
    {
//        self.window.viewsNeedDisplay = true;
        return kCVReturnSuccess;
    }
    
    public func startView() {
//        self.shared = [[ASShared alloc] initWithFrame:[self bounds]];
//        self.shared = ASShared.init(frame:window.rect)
        
//        let displayID: CGDirectDisplayID = CGMainDisplayID();
//
//        print("startView %@", self.shared);
//        print("Scaling factor \(self.window.backingScaleFactor)")
//
//        var error: CVReturn = kCVReturnSuccess;
//        error = CVDisplayLinkSetCurrentCGDisplay(displayLink, displayID)
//
//        if (error != 0)
//        {
//            print("DisplayLink created with error: \(error)");
//            displayLink = 0 as! CVDisplayLink
//        }
//
////        error = CVDisplayLinkSetOutputCallback(displayLink, renderCallback, nil)
//
//        CVDisplayLinkStart(displayLink);
    }
    
    public override func awakeFromNib()
    {
        print("Starting up");
        startView()
        startEverything(headyLifting: true)
        sharedReady()
        
        let appDelegate: AppDelegate =  NSApplication.shared.delegate as! AppDelegate;
            
        appDelegate.addShared(shared: shared)
    }
    
    public func menuQuit() {
        print("Quit from menu");
//        CVDisplayLinkStop(displayLink);
        self.quitProcedure()
    }
    
//    public func aboutDialog:(id) sender -> - (IBAction)
//    {
//        shared.about()
//    }
    
    public func acceptsFirstResponder() -> Bool
    {
        return true;
    }
    
    public func  becomeFirstResponder() -> Bool
    {
        return true;
    }
    
    public func resignFirstResponder() -> Bool
    {
        return true;
    }
    
    public func startEverything(headyLifting: Bool)
    {
//        let increments: NSSize
//        increments.height = 4;
//        increments.width = 4;
//        self.window.setContentResizeIncrements(increments: increments)
        if (headyLifting)
        {
            if shared.start() == false
            {
                print("Simulation initialization failed")
//                CVDisplayLinkStop(displayLink);
                quitProcedure()
                return
            }
        }
//        self.window?.makeKeyAndOrderFront(nil)
         
        NSApp.activate(ignoringOtherApps: true)
    }
    
    
    public func  keyUp(theEvent: NSEvent) {
        shared.keyUp()
    }
    
    public func  keyDown(theEvent: NSEvent) {
//        var local_key: Int = 0;
//        if ((theEvent.modifierFlags & NSEventModifierFlagControl) ||
//            (theEvent.modifierFlags & NSEventModifierFlagOption) {
//            local_key = 2048;
//        }
//
//        if theEvent.modifierFlags & NSEventModifierFlagNumericPad
//        {
//            /* arrow keys have this mask */
//            let theArrow: String = theEvent.charactersIgnoringModifiers
//             keyChar = 0;
//            if theArrow.length == 0 {
//                return            /* reject dead keys */
//            }
//            if theArrow.length == 1 {
////                let keyChar: unichar = theArrow.characterAtIndex:0];
//
//                let keyChar: uniChar = theArrow.utf8CString
//
//                if ( keyChar == NSLeftArrowFunctionKey )
//                {
//                    local_key += 28;
//                }
//                else if ( keyChar == NSRightArrowFunctionKey )
//                {
//                    local_key += 29;
//                }
//                else if ( keyChar == NSUpArrowFunctionKey )
//                {
//                    local_key += 30;
//                }
//                else if ( keyChar == NSDownArrowFunctionKey )
//                {
//                    local_key += 31;
//                }
//
//                shared.keyReceived(key: local_key)
//            }
//        }
//    if theEvent != nil
//        {
//            let first: NSRange = [[theEvent characters] rangeOfComposedCharacterSequenceAtIndex:0];
//            let match: NSRange = [[theEvent characters] rangeOfCharacterFromSet:[NSCharacterSet letterCharacterSet] options:0 range:first];
//            if (match.location != NSNotFound) {
//                unichar firstChar = [[theEvent characters] characterAtIndex:0];
//                NSCharacterSet *letters = [NSCharacterSet letterCharacterSet];
//                if ([letters characterIsMember:firstChar]) {
//                    // The first character is a letter in some alphabet
//                    [self.shared keyReceived:firstChar];
//                }
//            }
//        }
    }
    
    public func acceptsFirstMouse() -> Bool
    {
        return true;
    }
    
    public func mouseDown(theEvent: NSEvent)
    {
//        let location: NSPoint  = convertPoint(theEvent.locationInWindow fromView:nil)
//        [self.shared mouseOption:(([theEvent modifierFlags] & NSEventModifierFlagControl) || ([theEvent modifierFlags] & NSEventModifierFlagOption))];
//        [self.shared mouseReceivedWithXLocation:location.x yLocation:[self bounds].size.height - location.y];
    }
    
    public func rightMouseDown(theEvent: NSEvent)
    {
        mouseDown(theEvent: theEvent)
        shared.mouseOption(true)
    }
    
    public func otherMouseDown(theEvent: NSEvent)
    {
        rightMouseDown(theEvent: theEvent);
    }
    
    public func mouseUp(theEvent: NSEvent)
    {
        shared.mouseUp()
    }
    
    public func rightMouseUp(theEvent: NSEvent)
    {
        mouseUp(theEvent: theEvent)
    }
    
    public func otherMouseUp(theEvent: NSEvent)
    {
        mouseUp(theEvent: theEvent)
    }
    
    public func mouseDragged(theEvent: NSEvent)
    {
        mouseDown(theEvent: theEvent)
    }
    
    public func rightMouseDragged(theEvent: NSEvent)
    {
        rightMouseDown(theEvent: theEvent)
    }
    
    public func otherMouseDragged(theEvent: NSEvent)
    {
        rightMouseDown(theEvent: theEvent)
    }
    
    public func scrollWheel(theEvent: NSEvent)
    {
        shared.delta_x(theEvent.deltaX, delta_y: theEvent.deltaY)
    }
    
    public func  magnifyWithEvent(theEvent: NSEvent)
    {
        shared.zoom(theEvent.magnification)
    }
    
    public func rotateWithEvent(theEvent: NSEvent)
    {
        shared.rotation(Double(theEvent.rotation))
    }
    
}
